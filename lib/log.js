const config = require("../config")
const log4js = require('log4js');
const lg = log4js.getLogger();
log4js.configure({
  appenders: {
    out: {type: 'stdout'},
    file: {type: 'file', filename: config.logPath || './log/log.log'}
  },
  categories: {
    default: {
      appenders: ['out'], level: 'info'
    },
    nondefault: {
      appenders: ['file'], level: 'debug'
    }
  }
});
lg.level = config.logLevel || 'debug';
module.exports = lg;