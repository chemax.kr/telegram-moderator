const lg = require("./lib/log")
const config = require("./config")
const TelegramBot = require('node-telegram-bot-api');
const bot = new TelegramBot(config.botToken, {polling: true});

let options = {
    parse_mode: 'Markdown',
    reply_markup: JSON.stringify({
        inline_keyboard: [[
            {text: 'approve', callback_data: 'approve'}, {text: 'discard', callback_data: 'discard'}
        ]]
    })
}

bot.on('message', message => {
    let fileId = ""
    let fn = ""
    let chatId = config.moderatorId
    let localOptions = Object.assign({}, options)
    if (message.hasOwnProperty("video")) {
        fileId = message.video.file_id
        fn = "sendVideo"
    } else if (message.hasOwnProperty("photo")) {
        fileId = message.photo[message.photo.length - 1].file_id
        fn = "sendPhoto"
    } else if (message.hasOwnProperty("animation")) {
        fileId = message.animation.file_id
        fn = "sendAnimation"
    } else if (message.from.id !== config.moderatorId) {
        bot.sendMessage(message.chat.id, "Простите, я не понял формат сообщения. Я понимаю gif, видео и фото. Можете попробовать обратиться к модератору @voiddancer.").then()
        return
    } else {
        if (message.hasOwnProperty("reply_to_message") && message.from.id === config.moderatorId) {
            let localOptions = Object.assign({
                chat_id: message["reply_to_message"].chat.id,
                message_id: message["reply_to_message"].message_id
            }, options)
            bot.editMessageCaption(message.text, localOptions)
                .then()
                .catch(err => lg.error(err))
            bot.deleteMessage(message.chat.id, message.message_id)
                .then()
                .catch(err => lg.error(err))
        }
        return;
    }
    if (message.from.id === config.moderatorId) {
        lg.info("moderator message")
        chatId = config.chatId
        localOptions = {}
        bot.deleteMessage(message.chat.id, message.message_id).then().catch(err => lg.error(err))
    } else {
        if (message.from.username) {
            localOptions.caption = `#from @${message.from.username}`
        } else {
            localOptions.caption = `#from [${message.from.first_name}](tg://user?id=${message.from.id})`
        }
    }

    bot[fn](chatId, fileId, localOptions).then().catch(err => lg.error(err))

});

bot.on('callback_query', query => {
    const options = {}
    if (query.data === "approve") {
        if (!query.message.caption.startsWith("#from ")) {
            options.caption = query.message.caption
        }
        if (query.message.hasOwnProperty("video")) {
            bot.sendVideo(config.chatId, query.message.video.file_id, options).then().catch(err => lg.error(err))
        } else if (query.message.hasOwnProperty("photo")) {
            bot.sendPhoto(config.chatId, query.message.photo[query.message.photo.length - 1].file_id, options).then().catch(err => lg.error(err))
        } else if (query.message.hasOwnProperty("animation")) {
            bot.sendAnimation(config.chatId, query.message.animation.file_id, options).then().catch(err => lg.error(err))
        }
    }
    bot.deleteMessage(query.message.chat.id, query.message.message_id).then().catch(err => lg.error(err))
});
